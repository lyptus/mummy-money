from . import *
from .status import status


@mod.route('/clear')
def clear():
    Attempt.clear_all()
    Person.clear_all()
    State.clear_all()
    output = {}

    return output
