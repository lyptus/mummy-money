from . import *


@mod.route('/status')
def status():
    week = State.getWeek()
    people = People.all()
    attempts = Attempt.query.all()

    output = {
        "state": state_schema.dump(State.get()),
        "mummy_money": People.mummy_money(),
        "mummy_tree": [tree_schema.dump(People.get_tree(People.all(),
                                                        People.all()[0]))],
        "total_pop": len(people),
        "members": (len(people.active_members()) +
                    len(people.inactive_members())),
        "active_members": len(people.active_members()),
        "inactive_members": len(people.inactive_members()),
        "non_members": len(people.not_members()),
        "new_members": len(people.new_members(week)),
        "leaving_members": len(people.leaving_members(week)),
        "population": [person_schema.dump(p) for p in people],
        "history": ([attempt_schema.dump(a) for a in attempts
                     if a.investor_id is not None])
    }

    return jsonify(output)
