from . import *
from .status import status


@mod.route('/start')
def start():
    n = literal_eval(request.args.get('n') or '100')
    gaussian = literal_eval(request.args.get('gaussian') or 'False')
    nolimit = literal_eval(request.args.get('nolimit') or 'False')
    Attempt.clear_all()
    Person.clear_all()
    State.init()
    people = People.populate(n, gaussian, nolimit)
    People.initial_recruit()

    return status()
