from . import *
from .status import status


@mod.route('/simulate_week')
def simulate_week():
    people = People.all()
    print(State.getFinished())
    if (not State.getFinished()):
        People.recruit_cycle(people, State.incWeek())

        if (len(people.not_members()) == 0 or
                len(people.active_members()) == 0):
            State.setFinished(True)

    return status()
