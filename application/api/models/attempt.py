from application.api.database import db
from application.api.database.extended_model import ExtendedModel


class Attempt(ExtendedModel):
    __tablename__ = 'attempt'
    id = db.Column(db.Integer, primary_key=True)
    recruiter_id = db.Column(
        db.Integer, db.ForeignKey('person.id'), nullable=False)
    week = db.Column(db.Integer, nullable=False)
    rand_recruit = db.Column(db.Float, nullable=False)
    rand_invest = db.Column(db.Float)
    investor_id = db.Column(db.Integer, db.ForeignKey(
        'person.id'))
    recruiter = db.relationship(
        'Person', backref='recruit_attempts', foreign_keys=[recruiter_id])
    investor = db.relationship(
        'Person', backref='invest_attempts', foreign_keys=[investor_id])

    def __repr__(self):
        return '''{
    id: %s
    investor_id: %s,
    investor: %r,
    recruiter_id: %s,
    recruiter: %r,
    week: %s,
    rand_recruit: %s,
    rand_invest: %s
}''' % (self.id,
            self.investor_id,
            self.investor,
            self.recruiter_id,
            self.recruiter,
            self.week,
            self.rand_recruit,
            self.rand_invest)
