from application.api.database import db
from application.api.database.extended_model import ExtendedModel


class State(ExtendedModel):
    __tablename__ = 'state'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    week = db.Column(db.Integer, nullable=False)
    finished = db.Column(db.Boolean, nullable=False)
    nolimit = db.Column(db.Boolean, nullable=False)

    def __repr__(self):
        return '''{
    id: %s,
    week: %s,
    finished: %s
    nolimit: %s
}''' % (self.id, self.week, self.finished, self.nolimit)

    @classmethod
    def init(State):
        State.clear_all()
        state = State()
        state.week = 1
        state.finished = False
        state.nolimit = False
        state.upsert_this()
        return state

    @classmethod
    def setWeek(State, week: int):
        state = State.get()
        state.week = week
        state.upsert_this()

    @classmethod
    def getWeek(State):
        state = State.query.first()
        return state.week

    @classmethod
    def incWeek(State):
        week = State.getWeek() + 1
        State.setWeek(week)
        return week

    @classmethod
    def setFinished(State, finished: bool = True):
        state = State.get()
        state.finished = finished
        state.upsert_this()

    @classmethod
    def getFinished(State):
        state = State.query.first()
        return state.finished

    @classmethod
    def setNolimit(State, nolimit: bool = True):
        state = State.get()
        state.nolimit = nolimit
        state.upsert_this()

    @classmethod
    def getNolimit(State):
        state = State.query.first() or State.init()
        return state.nolimit

    @classmethod
    def get(State):
        return State.query.first() or State.init()
