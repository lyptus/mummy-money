import math
import random
import uuid
from typing import List, Optional

from application.api.database import db
from application.api.database.extended_model import ExtendedModel
from application.api.models.attempt import Attempt
from application.api.models.state import State

from flask import current_app


class Person(ExtendedModel):
    __tablename__ = 'person'
    id = db.Column(db.Integer, primary_key=True)
    innocence = db.Column(db.Float, nullable=False)
    experience = db.Column(db.Float, nullable=False)
    charisma = db.Column(db.Float, nullable=False)
    recruiter_id = db.Column(db.Integer, db.ForeignKey('person.id'))
    investors = db.relationship(
        'Person', backref=db.backref('recruiter', remote_side=[id]))
    first_week = db.Column(db.Integer)
    last_week = db.Column(db.Integer)

    def __repr__(person):
        return '''{
    id: %s,
    innocence: %f,
    experience: %f,
    charisma: %f,
    recruiter_id: %s,
    first_week: %s,
    last_week: %s
}''' % (person.id,
            person.innocence,
            person.experience,
            person.charisma,
            person.recruiter_id,
            person.first_week,
            person.last_week)

    def __init__(person,
                 gaussian: bool = False):
        # person.id = '%s' % (uuid.uuid4())
        person.innocence = Person.rand(gaussian)
        person.experience = Person.rand(gaussian)
        person.charisma = Person.rand(gaussian)

    @classmethod
    def rand(Person, gaussian: bool = False,
             min_max: (float, float) = (0.0, 1.0),
             mean: float = 0.5,
             sigma: float = 0.125):
        if gaussian:
            return random.gauss(mean, sigma)
        else:
            return random.uniform(min_max[0], min_max[1])

    @classmethod
    def random_pick(Person, population: list):
        return random.choice(population)

    @property
    def p_recruit(recruiter):
        return (recruiter.experience * recruiter.charisma *
                (1.0 - math.log(len(recruiter.investors) + 1, 10)))

    @property
    def p_invest(investor):
        return (investor.innocence * (1.0 - investor.experience))

    @property
    def max_weeks(member):
        if (State.getNolimit()):
            return 9999
        return math.floor(
            (1 - member.innocence) * member.experience * member.charisma * 10)

    @property
    def is_new(member):
        return State.getWeek() == member.first_week

    @property
    def is_leaving(member):
        return State.getWeek() == member.last_week

    @property
    def is_member(member):
        return member.first_week is not None

    @property
    def is_active(member):
        return member.first_week is not None and member.last_week is None

    @property
    def money(member):
        COMMISSION = current_app.config['MUMMY_MONEY_COMMISSION']
        INVESTMENT = current_app.config['MUMMY_MONEY_INVESTMENT']
        if (member.first_week is None):
            return 0
        if (member.first_week > 0):
            return len(member.investors)*COMMISSION - INVESTMENT
        else:
            return 0

    @property
    def text(member):
        if (member.id != 1):
            return ('%s: MM$: %s, INN: %.2f, CHA: %.2f, EXP: %.2f' %
                    (member.id,
                     member.money,
                     member.innocence,
                     member.charisma,
                     member.experience))
        else:
            return ('%s: MUMMY') % (member.id)

    def should_quit(recruiter,
                    week: int = -1):
        INVESTMENT = current_app.config['MUMMY_MONEY_INVESTMENT']
        if (((week - recruiter.first_week) >= recruiter.max_weeks) and
                (recruiter.money < 0)):
            recruiter.last_week = week
            recruiter.upsert_this()

    def recruit_attempt(recruiter,
                        population: list,
                        week: int = -1,
                        force: bool = False):
        attempt = Attempt()
        attempt.recruiter_id = recruiter.id
        attempt.week = week
        attempt.rand_recruit = Person.rand()
        if len(population) > 0:
            if force or attempt.rand_recruit < recruiter.p_recruit:
                candidate = Person.random_pick(population)
                attempt.rand_invest = Person.rand()
                investor = candidate.invest_attempt(recruiter, week,
                                                    attempt.rand_invest, force)
                if (investor is not None):
                    attempt.investor_id = investor.id
        attempt.upsert_this()
        recruiter.should_quit(week)
        return attempt

    def invest_attempt(candidate,
                       recruiter,
                       week: int = -1,
                       rand_invest: float = -1,
                       force: bool = False):
        if force or rand_invest < candidate.p_invest:
            candidate.first_week = week
            candidate.recruiter_id = recruiter.id
            candidate.upsert_this()
            return candidate

    def keys(person):
        return super().keys() + ['is_new', 'is_leaving', 'is_member',
                                 'is_active', 'money', 'p_recruit',
                                 'p_invest', 'max_weeks']
