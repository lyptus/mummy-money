import random
from typing import List

from application.api.models.person import Person
from application.api.models.attempt import Attempt
from application.api.models.state import State
from application.api.models.tree import Tree

from flask import current_app


class People(list):

    def not_members(population):
        return People([person for person
                       in population
                       if person.first_week is None])

    def active_members(population):
        return People([person for person
                       in population
                       if person.first_week is not None and
                       person.last_week is None])

    def inactive_members(population):
        return People([person for person
                       in population
                       if person.first_week is not None and
                       person.last_week is not None])

    def new_members(population,
                    week: int):
        return People([person for person
                       in population
                       if person.first_week is not None and
                       person.first_week == week])

    def leaving_members(population,
                        week: int):
        return People([person for person
                       in population
                       if person.first_week is not None and
                       person.last_week is not None and
                       person.last_week == week])

    def random_pick(population) -> Person:
        return random.choice(population)

    def recruit_cycle(population,
                      week: int):
        attempts = [investor.recruit_attempt(
            population.not_members(), week)
            for investor in population.active_members()]
        return population.new_members(week)

    def get_children(population, person: Person):
        return People([x for x
                       in population
                       if x.recruiter_id is not None and
                       x.recruiter_id == person.id])

    def get_descendants(population, person: Person):
        children = population.get_children(person)
        nested = People([population.get_descendants(child)
                         for child in children])
        flattened = [val for sublist in nested for val in sublist]
        return People(children + flattened)

    def get_tree(population, person: Person):
        children = population.get_children(person)
        nested = [population.get_tree(child) for child in children]
        return Tree(person, nested)

    @classmethod
    def populate(People, n: int = 100, gaussian: bool = False,
                 nolimit: bool = False):
        population = People([Person(gaussian) for i in range(n)])
        State.setNolimit(nolimit)
        mummy = population[0]
        mummy.first_week = 0
        Person.upsert(population)
        return population

    @classmethod
    def all(People):
        people = Person.query.all()
        return People(people)

    @classmethod
    def mummy(People):
        return Person.query.filter_by(first_week=0).first()

    @classmethod
    def mummy_money(People):
        INVESTMENT = current_app.config['MUMMY_MONEY_INVESTMENT']
        COMMISSION = current_app.config['MUMMY_MONEY_COMMISSION']
        mummy = People.mummy()
        people = People.all()
        n_members = len(people.get_descendants(mummy))
        return (INVESTMENT - COMMISSION) * n_members

    @classmethod
    def initial_recruit(People):
        mummy = People.mummy()
        week = 1
        for n in range(10):
            not_members = People.not_members(People.all())
            attempt = mummy.recruit_attempt(not_members, week, True)
            attempt.rand_recruit = 1
            attempt.rand_invest = 1
            attempt.upsert_this()
            people = People.all()
            if (len(people.not_members()) == 0 or
                    len(people.active_members()) == 0):
                State.setFinished(True)
        mummy.last_week = week
        mummy.upsert_this()
