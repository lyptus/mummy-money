from application.api.database import ma, db
from application.api.models.person import Person


class PersonSchema(ma.ModelSchema):
    class Meta:
        model = Person
        fields = ("id",
                  "is_new",
                  "is_leaving",
                  "is_member",
                  "is_active",
                  "money",
                  "innocence",
                  "experience",
                  "charisma",
                  "first_week",
                  "last_week",
                  "p_recruit",
                  "p_invest",
                  "max_weeks",
                  "recruiter",
                  "investors",
                  "recruit_attempts",
                  "invest_attempts",
                  "product",
                  "text")
        ordered = True


person_schema = PersonSchema()
people_schema = PersonSchema(many=True)
