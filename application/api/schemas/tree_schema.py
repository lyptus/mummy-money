from marshmallow import Schema
from marshmallow.fields import List, Nested, Str
from application.api.models.tree import Tree

from application.api.schemas.person_schema import PersonSchema


class TreeSchema(Schema):
    text = Str()
    member = Nested(PersonSchema)
    children = List(Nested('self'))

    class Meta:
        fields = ('text', 'member', 'children')
        ordered = True

tree_schema = TreeSchema()
