from application.api.database import ma
from application.api.models.attempt import Attempt


class AttemptSchema(ma.ModelSchema):
    class Meta:
        model = Attempt

attempt_schema = AttemptSchema()
attempts_schema = AttemptSchema(many=True)
