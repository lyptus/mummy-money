from application.api.database import ma
from application.api.models.state import State


class StateSchema(ma.ModelSchema):
    class Meta:
        model = State
        fields = ("week", "finished", "nolimit")
        ordered = True


state_schema = StateSchema()
