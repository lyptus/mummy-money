import os
from flask import Blueprint, current_app, render_template, send_file

mod = Blueprint('site', __name__, static_folder='dist/static',
                static_url_path='/static')


@mod.route('/')
def index_client():
    entry = os.path.join('site/dist', 'index.html')
    return send_file(entry)
