import Vue from 'vue'
import Vuex from 'vuex'
import pathify from './pathify'
import { make } from 'vuex-pathify';

Vue.use(Vuex)

const state = {
    open: [1],
    tree: {},
    status: {},
    n: 100,
    nolimit: false,
    gaussian: false,
    running: false,
    secs: 1,
    interval: undefined,
    person: undefined
}

const mutations = make.mutations(state)

const getters = make.getters(state)

const actions = make.actions(state)

const api_actions = {
    apiStatus: (context) => {
        Vue.axios
            .get(`/api/status`)
            .then(resp => context.dispatch('setStatus', resp.data));
    },
    apiStart: (context, ev, options = {
        n: state.n,
        gaussian: state.gaussian ? 'True' : 'False',
        nolimit: state.nolimit ? 'True' : 'False'
    }) => {
        console.log(options)
        Vue.axios
            .get(`/api/start?n=${options.n}&gaussian=${options.gaussian}&nolimit=${options.nolimit}`)
            .then(resp => {
                context.dispatch('setStatus', resp.data)
                console.log(resp.data);
            });
    },
    apiSimulate: (context) => {
        Vue.axios
            .get(`/api/simulate_week`)
            .then(resp => {
                context.dispatch('setStatus', resp.data)
                console.log(resp.data);
                console.log(state.running);
                console.log(resp.data.state.finished);
                if (state.running && resp.data.state.finished) {
                    context.dispatch('setRunning', false);
                    clearInterval(state.interval);
                    console.log('FINISHED');
                    console.log(state);
                }
            });
    },
    apiClear: (context) => {
        Vue.axios
            .get(`/api/clear`)
            .then(resp => {
                context.dispatch('setStatus', resp.data)
                console.log(resp.data);
                clearInterval(state.interval);
                context.dispatch('setRunning', false)
                context.dispatch('setPerson', undefined)
            });
    }
}

Object.assign(actions, api_actions)

console.log(actions)

export const store = new Vuex.Store({
    strict: true,
    plugins: [pathify.plugin],
    state,
    mutations,
    getters,
    actions
})