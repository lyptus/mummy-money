

FROM python:3.7

# Create app directory
WORKDIR /app

# Install app dependencies
COPY requirements.txt ./

RUN pip install -r requirements.txt

# Bundle app source
COPY . /app


EXPOSE 5000
CMD [ "python", "wsgi.py" ]



# # build
# FROM node:11.12.0-alpine as build-vue
# WORKDIR /app
# ENV PATH /app/node_modules/.bin:$PATH
# COPY ./application/site/package*.json ./
# RUN npm install
# COPY ./application/site .
# RUN npm run build


# # production
# FROM nginx:stable-alpine as production
# WORKDIR /app
# RUN apk update && apk add --no-cache python3 && \
#     python3 -m ensurepip && \
#     rm -r /usr/lib/python*/ensurepip && \
#     pip3 install --upgrade pip pipenv setuptools && \
#     if [ ! -e /usr/bin/pip ]; then ln -s pip3 /usr/bin/pip ; fi && \
#     if [[ ! -e /usr/bin/python ]]; then ln -sf /usr/bin/python3 /usr/bin/python; fi && \
#     rm -r /root/.cache
# RUN apk update && apk add gcc python3-dev musl-dev
# COPY --from=build-vue /app/dist /usr/share/nginx/html
# COPY ./nginx/default.conf /etc/nginx/conf.d/default.conf
# RUN pipenv install 
# RUN pipenv install gunicorn
# EXPOSE 5000
# CMD gunicorn -b 0.0.0.0:5000 wsgi:app --daemon && \
#       sed -i -e 's/$PORT/'"$PORT"'/g' /etc/nginx/conf.d/default.conf && \
#       nginx -g 'daemon off;'